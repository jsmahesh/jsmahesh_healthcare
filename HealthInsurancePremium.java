package org.emids.healthcare.service;

import org.emids.healthcare.model.CurrentHealth;
import org.emids.healthcare.model.Customer;
import org.emids.healthcare.model.Habits;

public interface HealthInsurancePremium {

	public int calculateHealthInsurancePremium(Customer cust);

	public double calculateInsurancePremiumAsPerAge(int age);

	public double calculateInsurancePremiumAsPerHealth(CurrentHealth health, double insurancePremium);

	public double calculateInsurancePremiumAsPerHabits(Habits habits, double insurancePremium);

}
