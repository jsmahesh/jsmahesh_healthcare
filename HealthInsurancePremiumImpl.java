package org.emids.healthcare.service;

import org.emids.healthcare.model.CurrentHealth;
import org.emids.healthcare.model.Customer;
import org.emids.healthcare.model.Habits;

public class HealthInsurancePremiumImpl implements HealthInsurancePremium {

	@Override
	public int calculateHealthInsurancePremium(Customer cust) {
		// TODO Auto-generated method stub
		double insurancePremium = 0;

		// Age Factor
		insurancePremium = calculateInsurancePremiumAsPerAge(cust.getAge());

		if (cust.getGender() == "Male")
			insurancePremium = insurancePremium + (insurancePremium * (2 / 100));

		insurancePremium = calculateInsurancePremiumAsPerHealth(cust.getCurrhealth(), insurancePremium);
		insurancePremium = calculateInsurancePremiumAsPerHabits(cust.getHabits(), insurancePremium);

		return (int) insurancePremium;
	}

	@Override
	public double calculateInsurancePremiumAsPerAge(int age) {

		double definsurancePremium = 5000;
		double insurancePremium = 0;

		// Age
		if (age < 18)
			insurancePremium = definsurancePremium;
		else if (age == 18 || age < 25)
			insurancePremium = definsurancePremium + (definsurancePremium * 0.1);
		else if (age == 25 || age < 30)
			insurancePremium = definsurancePremium + (definsurancePremium * 0.2);
		else if (age == 30 || age < 35)
			insurancePremium = definsurancePremium + (definsurancePremium * 0.3);
		else if (age == 35 || age < 40)
			insurancePremium = definsurancePremium + (definsurancePremium * 0.4);
		else if (age > 40)
			insurancePremium = definsurancePremium + (definsurancePremium * 0.6);

		System.out.println("After Age Consideration - Your Health Insurance Premium is :" + insurancePremium);

		return insurancePremium;
	}

	@Override
	public double calculateInsurancePremiumAsPerHealth(CurrentHealth health, double insurancePremium) {
		// TODO Auto-generated method stub
		if (health.getHyperTension() == "Yes")
			insurancePremium += (insurancePremium * 0.01);

		if (health.getBp() == "Yes")
			insurancePremium += (insurancePremium * 0.01);

		if (health.getBloodSugar() == "Yes")
			insurancePremium += (insurancePremium * 0.01);

		if (health.getOverWeight() == "Yes")
			insurancePremium += (insurancePremium * 0.01);

		System.out.println("After CurrentHealth Consideration - Your Health Insurance Premium is :" + insurancePremium);

		return insurancePremium;
	}

	@Override
	public double calculateInsurancePremiumAsPerHabits(Habits habits, double insurancePremium) {
		// TODO Auto-generated method stub

		if (habits.getDailyExercise() == "Yes")
			insurancePremium -= (insurancePremium * 0.03);

		if (habits.getAlchohol() == "Yes")
			insurancePremium += (insurancePremium * 0.03);

		if (habits.getDrugs() == "Yes")
			insurancePremium += (insurancePremium * 0.03);

		if (habits.getSmoking() == "Yes")
			insurancePremium += (insurancePremium * 0.03);

		System.out.println("After Habits Consideration - Your Health Insurance Premium is :" + insurancePremium);

		return insurancePremium;
	}

}
