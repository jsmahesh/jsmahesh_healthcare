package org.emids.healthcare.model;

public class Customer {

	private int id;
	private String name;
	private String gender;
	private int age;
	private CurrentHealth currHealth;
	private Habits habits;

	public Customer(String name, String gender, int age, CurrentHealth currHealth, Habits habits) {
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.currHealth = currHealth;
		this.habits = habits;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public CurrentHealth getCurrhealth() {
		return currHealth;
	}

	public void setCurrhealth(CurrentHealth currHealth) {
		this.currHealth = currHealth;
	}

	public Habits getHabits() {
		return habits;
	}

	public void setHabits(Habits habits) {
		this.habits = habits;
	}

}
