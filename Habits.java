package org.emids.healthcare.model;
// This class is for Insurance Customer Habits
public class Habits {

	private String smoking;
	private String alchohol;
	private String dailyExercise;
	private String drugs;
	
	public String getSmoking() {
		return smoking;
	}

	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

	public String getAlchohol() {
		return alchohol;
	}

	public void setAlchohol(String alchohol) {
		this.alchohol = alchohol;
	}

	public String getDailyExercise() {
		return dailyExercise;
	}

	public void setDailyExercise(String dailyExercise) {
		this.dailyExercise = dailyExercise;
	}

	public String getDrugs() {
		return drugs;
	}

	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}

	

	// Constructor Injection
	public Habits(String smoking, String alchohol, String dailyExercise, String drugs) {
		this.smoking = smoking;
		this.alchohol = alchohol;
		this.dailyExercise = dailyExercise;
		this.drugs = drugs;

	}

}
