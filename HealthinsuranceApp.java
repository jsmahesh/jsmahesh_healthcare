package org.emids.healthcare;

import org.emids.healthcare.model.CurrentHealth;
import org.emids.healthcare.model.Customer;
import org.emids.healthcare.model.Habits;
import org.emids.healthcare.service.HealthInsurancePremium;
import org.emids.healthcare.service.HealthInsurancePremiumImpl;

public class HealthinsuranceApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// This class is used to calculate health insurance premium
		HealthInsurancePremium healthInsuranceprem = (HealthInsurancePremium) new HealthInsurancePremiumImpl();

		CurrentHealth currHealthObj = new CurrentHealth("No", "No", "No", "Yes");
		Habits habitsObj = new Habits("No", "Yes", "Yes", "No");

		int inspre = healthInsuranceprem
				.calculateHealthInsurancePremium(new Customer("Norman Gomes", "Male", 34, currHealthObj, habitsObj));

		System.out.println(inspre);
	}

}
