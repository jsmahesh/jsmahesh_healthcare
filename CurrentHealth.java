package org.emids.healthcare.model;

public class CurrentHealth {

	private String hyperTension;
	private String bp;
	private String bloodSugar;
	private String overWeight;

	//// Constructor Injection
	public CurrentHealth(String hyperTension, String bp, String bloodSugar, String overWeight) {
		this.hyperTension = hyperTension;
		this.bp = bp;
		this.bloodSugar = bloodSugar;
		this.overWeight = overWeight;

	}

	public String getHyperTension() {
		return hyperTension;
	}

	public void setHyperTension(String hyperTension) {
		this.hyperTension = hyperTension;
	}

	public String getBp() {
		return bp;
	}

	public void setBp(String bp) {
		this.bp = bp;
	}

	public String getBloodSugar() {
		return bloodSugar;
	}

	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	public String getOverWeight() {
		return overWeight;
	}

	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

}
