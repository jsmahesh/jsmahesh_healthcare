package org.emids.healthcare.test;

import static org.junit.Assert.assertEquals;

import org.emids.healthcare.model.CurrentHealth;
import org.emids.healthcare.model.Customer;
import org.emids.healthcare.model.Habits;
import org.emids.healthcare.service.HealthInsurancePremium;
import org.emids.healthcare.service.HealthInsurancePremiumImpl;
import org.junit.Test;

public class HealthInsurancePremiumImplTest {

	HealthInsurancePremium healthInsuranceprem = (HealthInsurancePremium) new HealthInsurancePremiumImpl();

	@Test
	public void calculateHealthInsurancePremium() {

		CurrentHealth currHealthObj = new CurrentHealth("No", "No", "No", "Yes");
		Habits habitsObj = new Habits("No", "Yes", "Yes", "No");

		assertEquals(6559, healthInsuranceprem
				.calculateHealthInsurancePremium(new Customer("Norman Gomes", "Male", 34, currHealthObj, habitsObj)));

	}
}
